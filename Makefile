.PHONY: help build tag push refresh release

OWNER := europeanspallationsource
GIT_TAG := $(shell git describe --always)
IMAGE := pex


help:
# http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@echo "ics-docker-pex"
	@echo "=============="
	@echo
	@grep -E '^[a-zA-Z0-9_%/-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## build the latest image
	docker build -t $(OWNER)/$(IMAGE):latest .

tag: ## tag the latest image with the git tag
	docker tag $(OWNER)/$(IMAGE):latest $(OWNER)/$(IMAGE):$(GIT_TAG)

push: ## push the latest and git tag image
	docker push $(OWNER)/$(IMAGE):$(GIT_TAG)
	docker push $(OWNER)/$(IMAGE):latest

clean: ## remove the image with git tag
	-docker rmi $(OWNER)/$(IMAGE):$(GIT_TAG)

refresh: ## pull the latest image from Docker Hub
# skip if error: image might not be on dockerhub yet
	-docker pull $(OWNER)/$(IMAGE):latest

release: refresh \
	build \
	tag \
	push
release: ## build, tag, and push all stacks
