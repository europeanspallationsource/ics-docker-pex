FROM centos:7.3.1611

LABEL maintainer "benjamin.bertrand@esss.se"

# Install common dependencies
RUN  yum -y update \
  && yum -y install gcc make python-virtualenv libffi-devel openssl-devel \
  && yum clean all

WORKDIR /build

# Install pex
RUN virtualenv pex \
  && source pex/bin/activate \
  && pip install pex \
  && pex pex requests -c pex -o /usr/local/bin/pex \
  && deactivate \
  && rm -rf pex

ENTRYPOINT ["/usr/local/bin/pex"]
